var args = process.argv,
    file = args[2],
    act =  args[3] || "generate",
    isBlue = args[4],
    exec = require('child_process').exec,
    sys = require('sys'),
    fs = require('fs'),
    currentStory,
    execute = "",
    StoryNumber;
//
isBlue = (isBlue === "-blue")? ' -t "BLUE TASK"' : "";

var createStory = function(data){
    StoryNumber = data.match("US[0-9]+[^:\\W]");
    if(StoryNumber){
        StoryNumber = StoryNumber[0];
        currentStory = StoryNumber;
    }
    return StoryNumber;
}
//
if(file && file !== "help"){
    fs.readFileSync(file).toString().split('\n').forEach(function (line) { 
        var isStory = createStory(line);
        if (!isStory){
            if(line.match('[a-zA-Z0-9]', "g")){
                var l = line    .replace("-", "")
                                .replace(/\"/g, "\'");
                var action = 'ralio task create '+ currentStory +' -n \"'+ l +'\"'+isBlue+'\n'; 
                execute += action;
            }
        }
    });
}
//
var msg =   "\n\nCommands:\n"+
            "check                 - Loga a ação de envio sem postar o resultado que irá para o Rally \n"+
            "generate              - Cria as tasks de acordo com o TXT\n"+
            "\n"+
            "Options: \n" +
            "[command] blue        - Cria as tasks sinalizadas como blue task\n"+
            "\n";
//

console.log("processando...");

switch(act) {
    case "check":
        console.log(execute);  
        break;
    case "generate":
        exec(execute, function puts(error, stdout, stderr) { sys.puts(stdout) });
        break;
    case "help":
        console.log(msg)
        break;
    default:
        console.log(msg)
        break;
}