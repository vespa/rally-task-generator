# Rally Task Generator

## como usar:

Instale o node no seu ambiente. Feito isso, instale o Ralio como global

	$ npm install -g ralio

Configure seu usuário e senha no Rally
	
	$ ralio configure

Preencha o arquivo tasks.txt com a lista de histórias e suas tarefas no seguinte formato:

	US29766: Criar interface para inserção de imagens
	- configurar formulários
	- criar html
	- criar CSS
	- configurar comportamentos após envio do post

	US29768: Reindexar Veja SP
	- pegar lista de URLs
	- Rodar indexação
	- comparar lista atual com anterior
	- testes de relevância

É importante que o número da história seja sempre a primeira declaração do tópico.

Preenchida a lista, é só rodar o seguinte comando na pasta que está este script e a lista de tarefas:

	$ node generateTasks tasks.txt

E pronto. 

Caso você queira conferir se todas as tasks estão sendo devidamente enviadas para o Rally, você pode checar antes do envio com o seguinte comando:

	$ node generateTasks tasks.txt check

Em caso de dúvida, você também pode ver os comandos disponíveis usando o help:
	
	$ node generateTasks.js help


